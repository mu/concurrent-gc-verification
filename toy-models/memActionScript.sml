open HolKernel Parse boolLib bossLib;

open listTheory wordsLib finite_mapTheory

val _ = new_theory "memAction";
val _ = ParseExtras.tight_equality()

val _ = Datatype‘
  addr = <| n : num |>
    ;
  id = <| i : num |>
    ;
  mem_action = Read id addr num
             | Write addr (word8 list)
    ;
  memory = <| m : addr -> word8 |>
’;

val addr_component_equality = theorem "addr_component_equality"
val _ = overload_on("+", “λa i. a with n updated_by (+) i”)
val _ = overload_on ("-", “λa1 a2. a1.n - a2.n”)
val _ = overload_on ("<=", “λa1 a2. a1.n ≤ a2.n”)
val _ = overload_on ("<", “λa1 a2. a1.n < a2.n”)

val _ = overload_on("&", “λi. <| n := i |>”)

val addr_inj11 = Q.store_thm(
  "addr_inj11[simp]",
  ‘(&n : addr = &m) ⇔ (n = m)’,
  simp[addr_component_equality]);

val make_n = Q.store_thm(
  "make_n[simp]",
  ‘&(a.n) : addr = a’,
  simp[addr_component_equality]);

val addr_n_eq = Q.store_thm(
  "addr_n_eq",
  ‘(a.n = i ⇔ a = &i) ∧ (i = a.n ⇔ a = &i)’,
  simp[addr_component_equality]);

val addr_plus0 = Q.store_thm(
  "addr_plus0[simp]",
  ‘(a:addr) + 0 = a’,
  simp[Once addr_component_equality]);
val addr_add_assoc = Q.store_thm(
  "addr_add_assoc",
  ‘(a:addr) + x + y = a + (x + y)’,
  simp[addr_component_equality]);

val read_memory_def = Define‘
  read_memory a n mem = GENLIST (λi. mem.m (a + i)) n
’;

val write_memory_def = Define‘
  write_memory a [] m = m ∧
  write_memory a (b::bs) mem =
    write_memory (a + 1) bs mem with m updated_by (a =+ b)
’;
val _ = export_rewrites ["write_memory_def"]

val apply_action_def = Define‘
  apply_action (Read i a n) (mem,rds) = (mem, rds |+ (i, read_memory a n mem))∧
  apply_action (Write a bs) (mem,rds) = (write_memory a bs mem, rds)
’;

val read_write_lemma = Q.store_thm(
  "read_write_lemma",
  ‘∀bs a1 a2 mem.
     (write_memory a1 bs mem).m a2 =
       if a1 ≤ a2 ∧ a2 < a1 + LENGTH bs then EL (a2 - a1) bs
       else mem.m a2’,
  Induct_on `bs` >- simp[] >> rpt strip_tac >>
  reverse (Cases_on ‘a1 ≤ a2’)
  >- (simp[] >> ‘a1 ≠ a2’ suffices_by simp[combinTheory.UPDATE_APPLY] >>
      simp[addr_component_equality]) >>
  ‘∃d. a2.n = a1.n + d’ by metis_tac[arithmeticTheory.LESS_EQ_EXISTS] >>
  Cases_on ‘d’ >> fs[]
  >- (‘a1 = a2’ suffices_by simp[combinTheory.UPDATE_APPLY] >>
      simp[addr_component_equality]) >>
  ‘a2 ≠ a1’ by simp[addr_component_equality] >>
  simp[combinTheory.UPDATE_APPLY] >>
  qmatch_abbrev_tac ‘(if P then x else y) = (if Q then x else y)’ >>
  ‘P ⇔ Q’ suffices_by simp[] >>
  simp[Abbr‘P’, Abbr‘Q’]);

val read_write_TAKE = Q.store_thm(
  "read_write_TAKE",
  ‘∀bs a m n.
     n ≤ LENGTH bs ⇒
     read_memory a n (write_memory a bs m) = TAKE n bs’,
  simp[read_memory_def] >> rpt strip_tac >>
  irule listTheory.LIST_EQ >> simp[rich_listTheory.EL_TAKE, read_write_lemma]);

val action_extent_def = Define‘
  action_extent (Read _ a n) = {a + i | i < n} ∧
  action_extent (Write a bs) = {a + i | i < LENGTH bs}
’;

val actions_disjoint_def = Define‘
  (actions_disjoint (Read id1 _ _) (Read id2 _ _) ⇔ id1 ≠ id2) ∧
  (actions_disjoint a1 a2 ⇔
     DISJOINT (action_extent a1) (action_extent a2))
’;

val disjoint_actions_commute = Q.store_thm(
  "disjoint_actions_commute",
  ‘actions_disjoint a1 a2 ⇒
    apply_action a1 (apply_action a2 m) =
    apply_action a2 (apply_action a1 m)’,
  map_every Cases_on [`m`,`a1`,`a2`] >>
  simp[apply_action_def, actions_disjoint_def, action_extent_def]
  >- simp[finite_mapTheory.FUPDATE_COMMUTES]
  >- (strip_tac >>
      rename [‘read_memory rda n (write_memory wra bs m)’] >>
      ‘read_memory rda n (write_memory wra bs m) = read_memory rda n m’
         suffices_by simp[] >>
      simp[read_memory_def] >> irule listTheory.LIST_EQ >>
      simp[read_write_lemma] >>
      fs[pred_setTheory.DISJOINT_DEF, pred_setTheory.EXTENSION] >>
      rw[] >>
      rename [‘wra.n ≤ i + rda.n’] >>
      first_x_assum (qspec_then ‘rda + i’ strip_assume_tac)
      >- (first_x_assum (qspec_then `i` mp_tac) >> simp[]) >>
      ‘∃d. i + rda.n = wra.n + d’
        by metis_tac[arithmeticTheory.LESS_EQ_EXISTS] >>
      fs[] >> first_x_assum (qspec_then ‘d’ mp_tac) >>
      simp[addr_component_equality])
  >- (strip_tac >>
      rename [‘read_memory rda n (write_memory wra bs m)’] >>
      ‘read_memory rda n (write_memory wra bs m) = read_memory rda n m’
         suffices_by simp[] >>
      simp[read_memory_def] >> irule listTheory.LIST_EQ >>
      simp[read_write_lemma] >>
      fs[pred_setTheory.DISJOINT_DEF, pred_setTheory.EXTENSION] >>
      rw[] >>
      rename [‘wra.n ≤ i + rda.n’] >>
      first_x_assum (qspec_then ‘rda + i’ strip_assume_tac)
      >- (‘∃d. i + rda.n = wra.n + d’
            by metis_tac[arithmeticTheory.LESS_EQ_EXISTS] >>
          fs[] >> first_x_assum (qspec_then ‘d’ mp_tac) >>
          simp[addr_component_equality]) >>
      first_x_assum (qspec_then `i` mp_tac) >> simp[]) >>
  strip_tac >> simp[theorem "memory_component_equality", FUN_EQ_THM] >>
  simp[read_write_lemma] >> rw[] >>
  fs[pred_setTheory.EXTENSION, pred_setTheory.DISJOINT_DEF] >>
  rename [‘EL (b - a1) bs1 = EL (b - a2) bs2’] >>
  first_x_assum (qspec_then ‘b’ strip_assume_tac) >>
  rename [‘b ≠ aa + _’, ‘aa ≤ b’] >>
  ‘∃d. b.n = aa.n + d’ by metis_tac[arithmeticTheory.LESS_EQ_EXISTS] >>
  fs[] >> first_x_assum (qspec_then ‘d’ mp_tac) >>
  simp[addr_component_equality])


val _ = export_theory();
